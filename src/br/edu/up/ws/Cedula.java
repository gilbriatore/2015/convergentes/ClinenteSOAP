
package br.edu.up.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de cedula complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="cedula">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="candidato" type="{http://ws.up.edu.br/}candidato" minOccurs="0"/>
 *         &lt;element name="eleitor" type="{http://ws.up.edu.br/}eleitor" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cedula", propOrder = {
    "candidato",
    "eleitor"
})
public class Cedula {

    protected Candidato candidato;
    protected Eleitor eleitor;

    /**
     * Obt�m o valor da propriedade candidato.
     * 
     * @return
     *     possible object is
     *     {@link Candidato }
     *     
     */
    public Candidato getCandidato() {
        return candidato;
    }

    /**
     * Define o valor da propriedade candidato.
     * 
     * @param value
     *     allowed object is
     *     {@link Candidato }
     *     
     */
    public void setCandidato(Candidato value) {
        this.candidato = value;
    }

    /**
     * Obt�m o valor da propriedade eleitor.
     * 
     * @return
     *     possible object is
     *     {@link Eleitor }
     *     
     */
    public Eleitor getEleitor() {
        return eleitor;
    }

    /**
     * Define o valor da propriedade eleitor.
     * 
     * @param value
     *     allowed object is
     *     {@link Eleitor }
     *     
     */
    public void setEleitor(Eleitor value) {
        this.eleitor = value;
    }

}
