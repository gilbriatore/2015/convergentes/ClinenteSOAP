package br.edu.up.ws;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class ClienteWS {
	
	public static void main(String[] args) throws Exception {

		URL url= new URL("http://localhost:8080/urna/ws-soap?wsdl");
		QName qname= new QName("http://ws.up.edu.br/", "UrnaWSImplService");
		
		Service service = Service.create(url, qname);
		UrnaWS urna = service.getPort(UrnaWS.class);
		Candidato candidato = urna.getCandidato("11");
		//UrnaWSImplService urna = new UrnaWSImplService();
		//urna.getUrnaWSImplPort();
		
	}
}