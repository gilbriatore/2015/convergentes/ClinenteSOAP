
package br.edu.up.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.edu.up.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Cedula_QNAME = new QName("http://ws.up.edu.br/", "cedula");
    private final static QName _VotarResponse_QNAME = new QName("http://ws.up.edu.br/", "votarResponse");
    private final static QName _GetCandidatosClassificados_QNAME = new QName("http://ws.up.edu.br/", "getCandidatosClassificados");
    private final static QName _GetCandidatosClassificadosResponse_QNAME = new QName("http://ws.up.edu.br/", "getCandidatosClassificadosResponse");
    private final static QName _GetCedulaResponse_QNAME = new QName("http://ws.up.edu.br/", "getCedulaResponse");
    private final static QName _Votar_QNAME = new QName("http://ws.up.edu.br/", "votar");
    private final static QName _GetCandidato_QNAME = new QName("http://ws.up.edu.br/", "getCandidato");
    private final static QName _GetCandidatoResponse_QNAME = new QName("http://ws.up.edu.br/", "getCandidatoResponse");
    private final static QName _GetCedula_QNAME = new QName("http://ws.up.edu.br/", "getCedula");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.edu.up.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetCandidatosClassificados }
     * 
     */
    public GetCandidatosClassificados createGetCandidatosClassificados() {
        return new GetCandidatosClassificados();
    }

    /**
     * Create an instance of {@link Cedula }
     * 
     */
    public Cedula createCedula() {
        return new Cedula();
    }

    /**
     * Create an instance of {@link VotarResponse }
     * 
     */
    public VotarResponse createVotarResponse() {
        return new VotarResponse();
    }

    /**
     * Create an instance of {@link GetCandidato }
     * 
     */
    public GetCandidato createGetCandidato() {
        return new GetCandidato();
    }

    /**
     * Create an instance of {@link GetCandidatoResponse }
     * 
     */
    public GetCandidatoResponse createGetCandidatoResponse() {
        return new GetCandidatoResponse();
    }

    /**
     * Create an instance of {@link GetCedula }
     * 
     */
    public GetCedula createGetCedula() {
        return new GetCedula();
    }

    /**
     * Create an instance of {@link GetCedulaResponse }
     * 
     */
    public GetCedulaResponse createGetCedulaResponse() {
        return new GetCedulaResponse();
    }

    /**
     * Create an instance of {@link GetCandidatosClassificadosResponse }
     * 
     */
    public GetCandidatosClassificadosResponse createGetCandidatosClassificadosResponse() {
        return new GetCandidatosClassificadosResponse();
    }

    /**
     * Create an instance of {@link Votar }
     * 
     */
    public Votar createVotar() {
        return new Votar();
    }

    /**
     * Create an instance of {@link Candidato }
     * 
     */
    public Candidato createCandidato() {
        return new Candidato();
    }

    /**
     * Create an instance of {@link Eleitor }
     * 
     */
    public Eleitor createEleitor() {
        return new Eleitor();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cedula }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.up.edu.br/", name = "cedula")
    public JAXBElement<Cedula> createCedula(Cedula value) {
        return new JAXBElement<Cedula>(_Cedula_QNAME, Cedula.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VotarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.up.edu.br/", name = "votarResponse")
    public JAXBElement<VotarResponse> createVotarResponse(VotarResponse value) {
        return new JAXBElement<VotarResponse>(_VotarResponse_QNAME, VotarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCandidatosClassificados }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.up.edu.br/", name = "getCandidatosClassificados")
    public JAXBElement<GetCandidatosClassificados> createGetCandidatosClassificados(GetCandidatosClassificados value) {
        return new JAXBElement<GetCandidatosClassificados>(_GetCandidatosClassificados_QNAME, GetCandidatosClassificados.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCandidatosClassificadosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.up.edu.br/", name = "getCandidatosClassificadosResponse")
    public JAXBElement<GetCandidatosClassificadosResponse> createGetCandidatosClassificadosResponse(GetCandidatosClassificadosResponse value) {
        return new JAXBElement<GetCandidatosClassificadosResponse>(_GetCandidatosClassificadosResponse_QNAME, GetCandidatosClassificadosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCedulaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.up.edu.br/", name = "getCedulaResponse")
    public JAXBElement<GetCedulaResponse> createGetCedulaResponse(GetCedulaResponse value) {
        return new JAXBElement<GetCedulaResponse>(_GetCedulaResponse_QNAME, GetCedulaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Votar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.up.edu.br/", name = "votar")
    public JAXBElement<Votar> createVotar(Votar value) {
        return new JAXBElement<Votar>(_Votar_QNAME, Votar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCandidato }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.up.edu.br/", name = "getCandidato")
    public JAXBElement<GetCandidato> createGetCandidato(GetCandidato value) {
        return new JAXBElement<GetCandidato>(_GetCandidato_QNAME, GetCandidato.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCandidatoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.up.edu.br/", name = "getCandidatoResponse")
    public JAXBElement<GetCandidatoResponse> createGetCandidatoResponse(GetCandidatoResponse value) {
        return new JAXBElement<GetCandidatoResponse>(_GetCandidatoResponse_QNAME, GetCandidatoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCedula }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.up.edu.br/", name = "getCedula")
    public JAXBElement<GetCedula> createGetCedula(GetCedula value) {
        return new JAXBElement<GetCedula>(_GetCedula_QNAME, GetCedula.class, null, value);
    }

}
