
package br.edu.up.ws;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "UrnaWS", targetNamespace = "http://ws.up.edu.br/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface UrnaWS {


    /**
     * 
     * @return
     *     returns java.util.List<br.edu.up.ws.Candidato>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getCandidatosClassificados", targetNamespace = "http://ws.up.edu.br/", className = "br.edu.up.ws.GetCandidatosClassificados")
    @ResponseWrapper(localName = "getCandidatosClassificadosResponse", targetNamespace = "http://ws.up.edu.br/", className = "br.edu.up.ws.GetCandidatosClassificadosResponse")
    @Action(input = "http://ws.up.edu.br/UrnaWS/getCandidatosClassificadosRequest", output = "http://ws.up.edu.br/UrnaWS/getCandidatosClassificadosResponse")
    public List<Candidato> getCandidatosClassificados();

    /**
     * 
     * @param arg1
     * @param arg0
     */
    @WebMethod
    @RequestWrapper(localName = "votar", targetNamespace = "http://ws.up.edu.br/", className = "br.edu.up.ws.Votar")
    @ResponseWrapper(localName = "votarResponse", targetNamespace = "http://ws.up.edu.br/", className = "br.edu.up.ws.VotarResponse")
    @Action(input = "http://ws.up.edu.br/UrnaWS/votarRequest", output = "http://ws.up.edu.br/UrnaWS/votarResponse")
    public void votar(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        String arg1);

    /**
     * 
     * @param arg0
     * @return
     *     returns br.edu.up.ws.Cedula
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getCedula", targetNamespace = "http://ws.up.edu.br/", className = "br.edu.up.ws.GetCedula")
    @ResponseWrapper(localName = "getCedulaResponse", targetNamespace = "http://ws.up.edu.br/", className = "br.edu.up.ws.GetCedulaResponse")
    @Action(input = "http://ws.up.edu.br/UrnaWS/getCedulaRequest", output = "http://ws.up.edu.br/UrnaWS/getCedulaResponse")
    public Cedula getCedula(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns br.edu.up.ws.Candidato
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getCandidato", targetNamespace = "http://ws.up.edu.br/", className = "br.edu.up.ws.GetCandidato")
    @ResponseWrapper(localName = "getCandidatoResponse", targetNamespace = "http://ws.up.edu.br/", className = "br.edu.up.ws.GetCandidatoResponse")
    @Action(input = "http://ws.up.edu.br/UrnaWS/getCandidatoRequest", output = "http://ws.up.edu.br/UrnaWS/getCandidatoResponse")
    public Candidato getCandidato(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

}
